# Ansible Role: sshd

This role manages SSH daemon configuration.

## Mandatory variables

The following variables are mandatory:

```
sshd_allowtcpforwarding: "yes|no"
sshd_passwordauthentication: "yes|no"
sshd_x11forwarding: "yes|no"
```

## Optionally variables

To jail users inside their home using sftp define variables as follows:

```
sshd_jailed_users:
  - username: "username"
    path: "{{ apache_vhost_parent_directory }}/username"
```

## ToDo

- [x] Add sftp chails for users
- Add managed by ansible to sshd_config when it's available
